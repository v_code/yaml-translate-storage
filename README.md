# What is this
Working with YAML using the Java language. Creating a file with the .yaml extension, reading and writing data to add lines translation.

## Technologies
OOP, stream API, Jackson ObjectMapper, LanguageDetector (OptimaizeLangDetector)

## How to use
1. Change PATHNAME in the TranslationConfig class.
2. Create a yaml file using the createYamlFile() method.
3. Add text or texts to the yaml file using the addTranslation() method.

## Details
1. If a translation exists, it will be replaced.
2. If the translation contains only numbers or another language, then the translation will be written to "en".

### Automatically determined
- operating system (for the correct determination of the path to the file);
- the language of the transferred text for recording (there is no need to specify the locale, i.e. in which line to write the translation).

### Example: 
In a folder resources. It contains filenames, text IDs, and translations.