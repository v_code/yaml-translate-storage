package org.example.project;

public enum Language {

    DEFAULT("en"),
    EN("en"),
    RU("ru");

    private String locale;

    Language(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    @Override
    public String toString() {
        return "MyLanguages{" +
                "locale='" + locale + '\'' +
                '}';
    }
}
