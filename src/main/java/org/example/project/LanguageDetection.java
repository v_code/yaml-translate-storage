package org.example.project;

import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class LanguageDetection {

    public static String getTextLanguage(String text) {
        String language;
        try {
            Set<String> models = new HashSet<>();
            models.add(Language.EN.getLocale());
            models.add(Language.RU.getLocale());
            LanguageDetector detector = new OptimaizeLangDetector()
                    .loadModels(models);
            LanguageResult result = detector.detect(text);
            language = result.getLanguage();

        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
        return language;
    }

}
