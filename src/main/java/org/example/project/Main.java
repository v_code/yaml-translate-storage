package org.example.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static org.example.project.TranslationConfig.PATHNAME;

public class Main {
    public static void main(String[] args) {
        // 1. create yaml
        createYamlFile(PATHNAME);

        // 2. read yaml
        List<TranslationFile> readFiles = readYamlFile();

        // 3.1 add translate to yaml
        Message message = new Message("Сontinent"
                , "eurasia_continent"
                , "Eurasia");
        Message message1 = new Message("Ocean"
                , "pacific_ocean"
                , "Pacific Ocean");
        Message message2 = new Message("Сontinent"
                , "eurasia_continent"
                , "Евразия");

        System.out.println(addTranslation(readFiles, message));

        // 3.2 ... or add translates to yaml
        List<Message> messageList = new ArrayList<>();
        messageList.add(new Message("Culture"
                , "bushmen_culture"
                , "Bushmen"));
        messageList.add(new Message("Ocean"
                , "pacific_ocean"
                , "Тихий океан"));
        messageList.add(new Message("Continent"
                , "africa_continent"
                , "Africa"));

        messageList.stream()
                .peek(m -> addTranslation(readFiles, m))
                .collect(Collectors.toList());
    }

    private static String addTranslation(List<TranslationFile> readFiles
            , Message message) {
        String fileName = message.getFileName();
        String textID = message.getTextID();
        String translation = message.getTranslation();
        String language = LanguageDetection.getTextLanguage(translation);

        TranslationText translationText;
        List<TranslationText> translationTextList = new ArrayList<>();
        TranslationFile searchFile;
        List<TranslationFile> translationFileList = new ArrayList<>();

        if(!readFiles.isEmpty()){
            searchFile = findFile(readFiles, fileName);

            if(searchFile != null){
                translationText = findTextByTextId(searchFile, textID);

                if( translationText != null ){
                    if(language.equals(Language.RU.getLocale())){
                        translationText.setRu(translation);
                    } else {
                        translationText.setEn(translation);
                    }
                }

                if( translationText == null ){
                    translationTextList = searchFile.getTexts();
                    translationTextList.add(createTranslationText(language, textID, translation));
                }
                saveToFile(readFiles);
            }

            if(searchFile == null){
                translationTextList.add(createTranslationText(language, textID, translation));
                searchFile = new TranslationFile(fileName, translationTextList);
                readFiles.add(searchFile);
                saveToFile(readFiles);
            }
        }

        if(readFiles.isEmpty()){
            translationTextList.add(createTranslationText(language, textID, translation));
            searchFile = new TranslationFile(fileName, translationTextList);
            translationFileList.add(searchFile);

            saveToFile(translationFileList);
        }

        return translation;
    }

    private static TranslationText createTranslationText(String language
    , String textID, String translation){
        if(language.equals(Language.RU.getLocale())){
            return new TranslationText(textID, "", translation);
        }
        return new TranslationText(textID, translation, "");
    }


    private static TranslationFile findFile(List<TranslationFile> readFiles,
                                            String fileName){
        return readFiles.stream()
                .filter(t -> t.getFileName().equals(fileName))
                .findFirst().orElse(null);
    }

    private static TranslationText findTextByTextId(TranslationFile searchFile, String textID){
        return searchFile.getTexts().stream()
                .filter( t -> t.getTextID().equals(textID))
                .findFirst().orElse(null);
    }

    private static void saveToFile(List<TranslationFile> files){
        TranslationConfig config = new TranslationConfig(files);
        ObjectMapper om = new ObjectMapper(new YAMLFactory());

        try{
            om.writeValue(new File(PATHNAME), config);
            System.out.println("Success writing to " + PATHNAME);
        } catch (Exception exception){
            System.out.printf("Error in writing to " + exception.getMessage());
        }
    }

    private static List<TranslationFile> readYamlFile() {
        String[] arr = PATHNAME.split(createRegex());
        String resourceName = arr[arr.length-1];
        URL url = getUrl(resourceName);

        File file = new File(url.getFile());
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        TranslationConfig translationConfig;

        List<TranslationFile> fileList = new ArrayList<>();
        try{
            translationConfig = om.readValue(file, TranslationConfig.class);
            fileList = translationConfig.getFiles();
        } catch (Exception e){
            System.out.println("File empty");
        }
        return fileList;
    }

    public static String createRegex(){
        String os = System.getProperty("os.name");
        if(!os.equals("Linux")){
            return "\\\\\\\\";
        }
        return "/";
    }

    private static URL getUrl(String resourceName){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL url = classLoader.getResource(resourceName);

        if(url == null){
            throw new NoSuchElementException(resourceName);
        }
        return url;
    }

    public static void createYamlFile(String path) {
        try {
            File myObj = new File(path);
            if (myObj.createNewFile()) {
                System.out.println("Created " + myObj.getName() + " file");
            } else {
                System.out.println("File already exists");
            }
        } catch (IOException e) {
            System.out.println("Error in creating file");
            e.printStackTrace();
        }
    }

}