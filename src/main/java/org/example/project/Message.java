package org.example.project;

public class Message {
    private String fileName;
    private String textID;
    private String Translation;

    public Message() {}

    public Message(String fileName, String textID, String translation) {
        this.fileName = fileName;
        this.textID = textID;
        Translation = translation;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTextID() {
        return textID;
    }

    public void setTextID(String textID) {
        this.textID = textID;
    }

    public String getTranslation() {
        return Translation;
    }

    public void setTranslation(String translation) {
        Translation = translation;
    }

    @Override
    public String toString() {
        return "Message{" +
                "fileName='" + fileName + '\'' +
                ", textID='" + textID + '\'' +
                ", Translation='" + Translation + '\'' +
                '}';
    }
}
