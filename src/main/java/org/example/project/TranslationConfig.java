package org.example.project;

import java.util.List;

public class TranslationConfig {

    public final static String PATHNAME = "src/main/resources/translate.yaml";
    private List<TranslationFile> files;

    public TranslationConfig() {}

    public TranslationConfig(List<TranslationFile> files) {
        this.files = files;
    }

    public List<TranslationFile> getFiles() {
        return files;
    }

    public void setFiles(List<TranslationFile> files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return "MyConfig{" +
                "files=" + files +
                '}';
    }
}
