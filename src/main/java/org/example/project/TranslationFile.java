package org.example.project;

import java.util.List;

public class TranslationFile {

    private String fileName;
    private List<TranslationText> texts;

    public TranslationFile() {}

    public TranslationFile(String fileName, List<TranslationText> texts) {
        this.fileName = fileName;
        this.texts = texts;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<TranslationText> getTexts() {
        return texts;
    }

    public void setTexts(List<TranslationText> texts) {
        this.texts = texts;
    }


    @Override
    public String toString() {
        return "TranslationFile{" +
                "fileName='" + fileName + '\'' +
                ", texts=" + texts +
                '}';
    }
}
