package org.example.project;

public class TranslationText {
    private String textID;
    private String en;
    private String ru;

    public TranslationText() {}

    public TranslationText(String textID, String en, String ru) {
        this.textID = textID;
        this.en = en;
        this.ru = ru;
    }

    public String getTextID() {
        return textID;
    }

    public void setTextID(String textID) {
        this.textID = textID;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }

    @Override
    public String toString() {
        return "TextTranslate{" +
                "textID='" + textID + '\'' +
                ", en='" + en + '\'' +
                ", ru='" + ru + '\'' +
                '}';
    }
}
